# Containerized versions of Octopus Spack toolchains

The availabe container images currently are

- `releases-23b`  
  contains the
  `foss2022a-serial`,
  `foss2022a-mpi`,
  `foss2022a-cuda-mpi`
  toolchains

- `releases-24a`  
  contains the
  `foss2023a-serial`,
  `foss2023a-mpi`,
  `foss2022a-cuda-mpi`,
  `intel2023a-mpi`
  toolchains

- `releases-25a`  
  contains the
  `gcc-12_3_0`,
  `gcc-12_3_0-openmpi-4_1_5`,
  `gcc-13_2_0`,
  `gcc-13_2_0-openmpi-4_1_6`,
  `gcc-13_2_0-openmpi-4_1_6-cuda-12_6_2`,
  `gcc-14_2`,
  `gcc-14_2-openmpi-5_0_5`,
  `intel-2021_10_0`,
  `intel-2021_10_0-impi-2021_10_0`
  toolchains

- `opencl`  
  based on `releases-24a` but with the additional packages `clblas`, `clblast`,
  and `clfft` installed into the `foss2022a-cuda-mpi` toolchain and additional
  system packages and environment variables to allow for execution of OpenCL.

## Using the container images

To start developing inside the container, it should be run in interactive mode
(`-it`) and with a login shell (`/bin/bash -l`) to be able to load modules.

```console
$ podman run -it --rm -v "$PWD":/__w:z -w /__w gitlab-registry.mpcdf.mpg.de/menkeh/spack-dockerized:releases-24a /bin/bash -l
root@99690b749bfa:/__w# module -t avail  
/spack-environments-releases-24a/lmod/Core:
gcc/
gcc/11.3.0
gcc/12.3.0
toolchain/
toolchain/foss2022a-cuda-mpi
toolchain/foss2023a-mpi
toolchain/foss2023a-serial
/usr/share/lmod/lmod/modulefiles:
Core/
Core/lmod
Core/settarg
```

If the NVIDIA container runtime is installed and correctly set up, it is also
possible to run CUDA and OpenCL code inside the container. To install it on
Fedora Workstation see [@ai-ml/nvidia-container-toolkit on
COPR](https://copr.fedorainfracloud.org/coprs/g/ai-ml/nvidia-container-toolkit/)

```console
$ podman run --gpus all -it --rm -v "$PWD":/__w:z -w /__w gitlab-registry.mpcdf.mpg.de/menkeh/spack-dockerized:opencl /bin/bash -l
root@0b5c66032def:/__w# nvidia-smi -L
GPU 0: NVIDIA GeForce GT 1030 (UUID: GPU-2ab44e55-1d86-667f-c064-79733248b243)
root@0b5c66032def:/__w# clinfo --list
Platform #0: NVIDIA CUDA
 `-- Device #0: NVIDIA GeForce GT 1030
```

## Compiling Octopus inside the container

To this end I like having an extra file that loads all the modules and sets some
convenient environment variables for me.

`Modulefile`: This loads the modules and sets compiler flags up to use with AddressSantizier.

```bash
module purge
module load toolchain/foss2023a-mpi octopus-dependencies/full
unset CPATH LIBRARY_PATH
export CFLAGS="-O2 -fno-unroll-loops -g3 -fno-omit-frame-pointer -fsanitize=address"
export CXXFLAGS="-O2 -fno-unroll-loops -g3 -fno-omit-frame-pointer -fsanitize=address"
export FFLAGS="-O2 -fno-unroll-loops -g3 -fno-omit-frame-pointer -fno-var-tracking-assignments -fsanitize=address"
export LDFLAGS="-fsanitize=address"
export ASAN_OPTIONS="symbolize=1"
export LSAN_OPTIONS="suppressions=/__w/lsan.supp:print_suppressions=1"
export OMPI_MCA_memory="^patcher" # https://github.com/open-mpi/ompi/issues/12819
export CMAKE_CONFIGURE_EXTRA_ARGS="-DCMAKE_DISABLE_FIND_PACKAGE_DftbPlus=true -DCMAKE_REQUIRE_FIND_PACKAGE_DftbPlus=false -DCMAKE_DISABLE_FIND_PACKAGE_BerkleyGW=true -DCMAKE_REQUIRE_FIND_PACKAGE_BerkleyGW=false"
export PATH="/__w/_build:${PATH//"/__w/_build:"}"
export OCTOPUS_SHARE="/__w/_build/share"
alias gdb='env ASAN_OPTIONS="abort_on_error=1${ASAN_OPTIONS:+:$ASAN_OPTIONS}" gdb'
```

This also requires another file containing suppressions for LeakSanitizer.

`lsan.supp`:

```
# https://github.com/open-mpi/ompi/issues/12584
leak:libmpi.*
leak:libopen-pal.*
leak:libpmix.*
leak:hwloc_bitmap_alloc
leak:__vasprintf_internal
```
